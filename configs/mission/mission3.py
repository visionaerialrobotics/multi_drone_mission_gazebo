#!/usr/bin/env python2

import executive_engine_api as api
import rospy
'''
This is a simple mission, the drone takes off, follows a path and lands
There is a little detail in the way of activating the behaviors:
1.activateBehavior. Activates the behavior and let other behaviors run.
2.executeBehavior. Activates the behavior and do not let other behaviors run.
'''
def runMission():
  print("Starting mission...")

  print("Paying attention to robots...")
  api.activateBehavior('PAY_ATTENTION_TO_ROBOT_MESSAGES')

  print("informing position to robots...")
  api.activateBehavior('INFORM_POSITION_TO_ROBOTS')

  print("Taking off...")

  api.executeBehavior('TAKE_OFF')

  print("Following path")

  api.executeBehavior('FOLLOW_PATH', path=[ [0, -3, 1],[2,1,4] ])

  print("Inform situation")
  api.executeBehavior('INFORM_ROBOTS', destination="drone112", text="from_113_to_112_first")

  print("Inform situation")
  api.executeBehavior('INFORM_ROBOTS', destination="drone112", text="from_113_to_112_second")

  print("Inform situation")
  api.executeBehavior('INFORM_ROBOTS', destination="drone111", text="from_113_to_111_first")

  print("Inform situation")
  api.executeBehavior('INFORM_ROBOTS', destination="drone111", text="from_113_to_111_second")

  print("Landing")

  result = api.executeBehavior('LAND')
  print('Finishing mission...')
