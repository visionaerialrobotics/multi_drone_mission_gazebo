#!/usr/bin/env python3

import mission_execution_control as mxc
import rospy
import time

def followPath(ruta):
  for point in ruta:
    goToPoint(point)

def goToPoint(point):
  while True:
    activation_succesful, result  = mxc.executeTask('FOLLOW_PATH', path=[point])
    print(result)
    if(result != 5 ):# result=5 means INTERRUPTED
      break

def mission():
  print("Starting mission...")

  print("PID_MOTION_CONTROL")
  mxc.startTask('PID_MOTION_CONTROL')

  print("Paying attention to robots...")
  mxc.startTask('PAY_ATTENTION_TO_ROBOT_MESSAGES')

  print("informing position to robots...")
  mxc.startTask('INFORM_POSITION_TO_ROBOTS')

  print("Taking off...")
  mxc.executeTask('TAKE_OFF')

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', receiver="drone111", message="from_112_to_111")

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', receiver="drone111", message="from_112_to_111_el_segundo")

  print("Following path")
  followPath([ [6, 1, 1] ,[-3,1,1]])

  print("Landing")
  mxc.executeTask('LAND')

  print('Finishing mission...')
