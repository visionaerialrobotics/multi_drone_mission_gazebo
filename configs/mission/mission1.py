#!/usr/bin/env python3

import mission_execution_control as mxc
import rospy

def followPath(ruta):
  for point in ruta:
    goToPoint(point)

def goToPoint(point):
  while True:
    activation_succesful, result  = mxc.executeTask('FOLLOW_PATH', path=[point])
    print(result)
    if(result != 5 ):# result=5 means INTERRUPTED
      break

def mission():
  print("Starting mission...")

  print("PID_MOTION_CONTROL")
  mxc.startTask('PID_MOTION_CONTROL')

  print("Paying attention to robots...")
  mxc.startTask('PAY_ATTENTION_TO_ROBOT_MESSAGES')

  print("informing position to robots...")
  mxc.startTask('INFORM_POSITION_TO_ROBOTS')

  print("Taking off...")
  mxc.executeTask('TAKE_OFF')

  print("Following path")
  followPath([[-2, 1, 1] ,[4,1,1]])

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', receiver="drone112", message="from_111_to_112")

  print("Inform situation")
  mxc.executeTask('INFORM_ROBOTS', receiver="drone112", message="from_111_to_112_el_segundo")

  print("Landing")
  mxc.executeTask('LAND')

  print('Finishing mission...')
