##  Multi drone mission rotors simulator

In order to install and execute this project, perform the following steps:

### Install the project

- Download the installation files:

        $ git clone https://bitbucket.org/visionaerialrobotics/aerostack_installers.git ~/temp

- Run the following installation script to install the project "multi_drone_mission_rotors_simulator":

        $ ~/temp/install_project_from_source.sh projects/multi_drone_mission_rotors_simulator

### Execute the project

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects/multi_drone_mission_rotors_simulator

- Execute the script that launches Gazebo:

        $ ./launcher_gazebo.sh

- Open a new terminal and change directory to the project:

        $ cd $AEROSTACK_STACK/projects/multi_drone_mission_rotors_simulator

- Execute the scripts that launch the Aerostack components for this project:
 
        $ ./main_launcher.sh

- To stop the processes execute the following script:

        $ ./stop.sh

- To close the inactive terminals:

        $ killall bash
